package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class _Tests {
    @Given("the Maker has chosen a word")
    public void today_is_sunday() {
        Class c = this.getClass();          // if you want to use the current class

        System.out.println("Package: "+c.getPackageName()+"\nClass: "+c.getSimpleName()+"\nFull Identifier: "+c.getName());    }
    @When("the Breaker makes a guess")
    public void i_ask_whether_it_s_friday_yet() {
        System.out.println("2");    }
    @Then("the Maker is asked to score")
    public void i_should_be_told() {
        System.out.println("3");
    }
}
